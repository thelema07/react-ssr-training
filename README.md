# React Server Side Rendering Testing App for La Nacion

- How to run Client Side :

1. Go to console and run in app folder "npm start".
2. Go to browser and open "http://localhost:3000/";
3. You should see the main app in development client-side mode.

- How to run Server Side :

1. Build the app by running on console "npm run dev"
2. You must modify the builer script in package.json accordint to your OS:
   linux: "dev:build-server": "NODE_ENV=development webpack --config webpack.server.js --mode=development -w",
   windows: "dev:build-server": "SET NODE_ENV=development webpack --config webpack.server.js --mode=development -w"

3. Go to console and run "npm run dev", this will generate the server-side app folder.
4. Make sure you install npm serve package : "npm i -g serve".
5. Finally run in another console "npm run watch".
6. Go to browser and open "http://localhost:5000/".
7. Voilá, you should see a server side rendered app.
