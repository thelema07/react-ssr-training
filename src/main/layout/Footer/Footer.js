import React from "react";
import logo from "../../../img/logo-la-nacion.png";
import googleBadge from "../../../img/google-play-badge.png";
import appleBadge from "../../../img/apple-store-badge.png";
import "./Footer.css";

const Footer = () => {
  return (
    <>
      <div className="footer">
        <div className="footer-icon-top">
          <div className="footer-block-icon">
            <i className="fa fa-facebook footer-icon-single"></i>
            <i className="fa fa-twitter footer-icon-single"></i>
            <i className="fa fa-instagram footer-icon-single"></i>
            <i className="fa fa-rss footer-icon-single"></i>
          </div>
          <div className="footer-block-logo">
            <img className="img-logo-small" src={logo} alt="Logo" />
          </div>
          <div className="footer-block-app-icons">
            <img className="img-google-small" src={googleBadge} alt="Logo" />
            <img className="img-apple-small" src={appleBadge} alt="Logo" />
          </div>
        </div>

        <div className="footer-bottom">
          <div className="footer-links-left">
            <a
              className="ln-link"
              href="http://especiales.lanacion.com.ar/varios/mapa-sitio/index.html"
            >
              Mapa del sitio
            </a>
            <a
              className="ln-link"
              href="https://micuenta.lanacion.com.ar/ayuda"
            >
              Ayuda
            </a>
            <a className="ln-link" href="https://micuenta.lanacion.com.ar/tyc">
              Términos y Condiciones
            </a>
            <a className="ln-link" href="https://micuenta.lanacion.com.ar/tyc">
              Subscribirse al diario impreso
            </a>
          </div>
          <div className="footer-links-right">
            <a
              className="ln-link-blk"
              href="http://especiales.lanacion.com.ar/varios/mapa-sitio/index.html"
            >
              Protegido por re CAPTCHA
            </a>
            <a
              className="ln-link"
              href="https://micuenta.lanacion.com.ar/ayuda"
            >
              Condiciones
            </a>
            <a className="ln-link" href="https://micuenta.lanacion.com.ar/tyc">
              Privacidad
            </a>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
