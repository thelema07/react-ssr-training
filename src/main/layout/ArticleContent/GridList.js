import React, { useEffect, useState } from "react";
import { loadArticles } from "../../api/axiosRequest";
import GridItem from "./GridItem";
import "./GridList.css";

const GridList = () => {
  const [articles, setArticles] = useState([]);

  useEffect(() => {
    loadArticles().then(response => {
      setArticles(response);
    });
  }, [articles, setArticles]);

  return (
    <>
      <div className="grid-list">
        {articles.map(card => (
          <div className="grid-item" key={card.date}>
            <img className="grid-image" src={card.image} alt={card.title} />
            <h6 className="grid-headline">{card.title}</h6>
            <h6 className="grid-article-date">{card.date}</h6>
          </div>
        ))}
      </div>
    </>
  );
};

export default GridList;
