import React from "react";

const GridItem = card => (
  <div className="grid-item" key={card.date}>
    <img className="grid-image" src={card.image} alt={card.title} />
    <h6>{card.title}</h6>
    <h6>{card.title}</h6>
  </div>
);

export default GridItem;
