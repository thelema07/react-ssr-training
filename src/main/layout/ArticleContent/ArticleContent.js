import React from "react";
import GridList from "./GridList";
import "./ArticleContent.css";

const ArticleContent = () => {
  return (
    <>
      <div className="adds-top-page">
        <div className="add-bar"></div>
      </div>
      <div className="article-layout">
        <div className="article-display">
          <h1 className="article-main">Acumulado Grilla</h1>
          <ul className="article-list" style={{ display: "flex" }}>
            <li>Platos Principales</li>
            <li style={{ marginLeft: "1.5rem" }}>Cerdo</li>
            <li style={{ marginLeft: "1.5rem" }}>Papas</li>
            <li style={{ marginLeft: "1.5rem" }}>Date un gustito</li>
            <li style={{ marginLeft: "1.5rem" }}>La familia</li>
          </ul>

          <GridList />
        </div>
        <div className="side-add-display"></div>
      </div>
    </>
  );
};

export default ArticleContent;
