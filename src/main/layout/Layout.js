import React from "react";
import Header from "./Header/Header";
import Footer from "./Footer/Footer";
import ArticleContent from "./ArticleContent/ArticleContent";

const Layout = () => {
  return (
    <>
      <Header />
      <ArticleContent />
      <Footer />
    </>
  );
};

export default Layout;
