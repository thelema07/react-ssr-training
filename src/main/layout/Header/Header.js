import React, { useState, useEffect } from "react";
import logo from "../../../img/logo-la-nacion.png";
import "./Header.css";

// TODO
// Create three divs and make them responsive, in the center we have out logo
// An scrolltop function triggers when scroll down

const Header = () => {
  const [toggleMenu, setToggleMenu] = useState(false);
  const switchMenu = () => setToggleMenu(!toggleMenu);

  useEffect(() => {}, []);

  return (
    <>
      <header className="main-header">
        <div className="head-block-search">
          <div className={`switch-icon ${toggleMenu ? "clicker-change" : ""}`}>
            <div onClick={switchMenu}>
              <div className="bar1"></div>
              <div className="bar2"></div>
              <div className="bar3"></div>
            </div>
            <div className="icon-title">{`${
              toggleMenu ? "Menu" : "Cerrar"
            }`}</div>
            <div className="input-container">
              <input
                className="input-field"
                type="text"
                placeholder="Buscar"
                name="usrnm"
              />
              <i className="fa fa-search icon"></i>
            </div>
          </div>
        </div>
        <div className="head-block-logo">
          <img className="img-logo" src={logo} alt="Logo" />
        </div>
        <div className="head-block-links">
          <button className="btn-subscribe">Suscribite</button>
          <button className="btn-login">Ingresar</button>
        </div>
      </header>
    </>
  );
};

export default Header;
