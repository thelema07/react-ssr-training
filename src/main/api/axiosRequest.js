import Axios from "axios";

const url = (Axios.defaults.baseURL = "https://api-test-ln.herokuapp.com");

export const loadArticles = async () => {
  try {
    const res = await Axios.get(url + "/articles");

    const response = [];

    res.data.articles.forEach(article => {
      if (article.subtype === "7") {
        response.push({
          date: dateFormat(article.display_date),
          title: article.headlines.basic,
          image: article.promo_items.basic.url
        });
      }
    });

    console.log(response);
    return response;
  } catch (error) {
    return error;
  }
};

const dateFormat = date => {
  const formatedDate = new Date(date);
  console.log(formatedDate);
  const monthNames = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre"
  ];

  const year = formatedDate.getFullYear();
  const month = formatedDate.getMonth();
  const day = formatedDate.getDate();
  const dateString = `${day} de ${monthNames[month]} de ${year}`;

  return dateString;
};
